


const http = require("http");

let port = 3000;


const server = http.createServer((request, response) => {

	if(request.url == '/login'){
		// method of the response object that allows us to set status codes and content types.
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome to the login page.');
	}

	else{
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}


}).listen(3000);

console.log('Welcome to the login page');